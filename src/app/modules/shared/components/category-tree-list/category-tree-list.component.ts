import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {CategoryModel} from '../../models/category.model';
import {tap} from 'rxjs/operators';
import {CategoryService} from '../../../../services/category.service';

@Component({
  selector: 'app-category-tree-list',
  templateUrl: './category-tree-list.component.html',
  styleUrls: ['./category-tree-list.component.scss'],
})
export class CategoryTreeListComponent implements OnChanges {
  @Input() multiple: boolean;
  @Input() expanded: boolean;
  @Input() editable: boolean;
  @Input() fullHeight: boolean;
  @Input() newCategory: boolean;
  @Input() selectedCategories: number[] = [];
  @Output() selectedCategory = new EventEmitter();
  @Input() title: string;
  @Input() selectedCategoryName: string;
  keyword: string;
  loadingState: boolean;
  categories: CategoryModel[];

  constructor(private _category: CategoryService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.title) {
      this.title = 'CPV tree list';
    }
    this.getData();
  }

  getData() {
    this.loadingState = true;
    if (!this.newCategory) {
      this._category.getParent().subscribe(
        (r: CategoryModel[]) => {
          this.loadingState = false;
          r.forEach(c => (c.hasChild = true));
          if (this.selectedCategories && this.selectedCategories.length > 0) {
            this.categories = r.map((c: CategoryModel) => {
              return {...c, checked: this.selectedCategories.includes(c.id)};
            });
          } else {
            this.categories = r;
          }
        },
        error => (this.loadingState = false)
      );
    } else {
      this._category.getInternalCategories(false).subscribe(
        (r: CategoryModel[]) => {
          this.loadingState = false;
          this.categories = r.map(c => {
            c.subCategories = c.subCategories.map(sc => {
              return {...sc, checked: this.selectedCategories.includes(sc.id), collapsed: false};
            });
            return {...c, hasChild: c.subCategories.length > 0, checked: this.selectedCategories.includes(c.id), collapsed: false};
          });
        },
        error => (this.loadingState = false)
      );
    }
  }
  getChild(category: CategoryModel) {
    if (!this.newCategory) {
      this.loadingState = true;
      if (category.subCategories === null || category.subCategories.length === 0) {
        this._category.getChild(category.id).subscribe(
          (r: CategoryModel[]) => {
            this.loadingState = false;
            if (this.selectedCategories && this.selectedCategories.length > 0) {
              [...category.subCategories] = r.map((c: CategoryModel) => {
                return {...c, checked: this.selectedCategories.includes(c.id)};
              });
            } else {
              [...category.subCategories] = r;
            }
          },
          error => (this.loadingState = false)
        );
      } else {
        this.loadingState = false;
        [...category.subCategories] = [];
      }
    }
  }
  filterCategories() {
    if (this.keyword !== '') {
      this.loadingState = true;
      this._category
        .filterCategories(this.newCategory ? 'internalCategory/SearchInternalCategories' : 'category/SearchInCategories', {
          keyword: this.keyword,
          pageNumber: 1,
          pageSize: 100,
          active: false,
        })
        .pipe(tap(() => (this.loadingState = false), () => (this.loadingState = false)))
        .subscribe((res: any) => {
          this.newCategory ? (this.categories = res.internalCategories) : (this.categories = res.cpvCodes);
        });
    } else {
      this.getData();
    }
  }
}
