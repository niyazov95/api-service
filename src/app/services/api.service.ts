import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {LayoutService} from './layout.service';
import {Success} from '../shared/classes/success.class';
import {SharedService} from './shared.service';

@Injectable({
  providedIn: 'root',
})
export class APIService {
  constructor(private httpClient: HttpClient, private layoutService: LayoutService, private sharedService: SharedService) {
    this.apiUrl = environment.apiUrl;
  }
  private readonly apiUrl: string;
  // private getFullUrl(url: string) {
  //   return `${this.apiUrl}${url}`;
  // }
  getFullUrl(url: string, apiUrl: string) {
    return `${apiUrl}${url}`;
  }
  get<T>(url: string, params?: HttpParams, apiUrl = this.apiUrl): Observable<T> {
    const fullUrl = this.getFullUrl(url, apiUrl);
    return this.proceedFullRequest<T>(this.httpClient.get<T>(this.getFullUrl(url, apiUrl), this.getOptions(params)), fullUrl);
  }
  post<T, R>(url: string, requestBody: R, params?: HttpParams, apiUrl = this.apiUrl): Observable<T> {
    const fullUrl = this.getFullUrl(url, apiUrl);
    return this.proceedFullRequest<T>(this.httpClient.post<T>(fullUrl, requestBody, this.getOptions(params)), fullUrl);
  }
  delete<T>(url: string, identifier: number | string, params?: HttpParams, apiUrl = this.apiUrl): Observable<T> {
    const fullUrl = this.getFullUrl(url, apiUrl);
    return this.proceedFullRequest<T>(this.httpClient.delete<T>(`${fullUrl}${identifier}`, this.getOptions(params)), fullUrl);
  }
  put<T, R>(url: string, requestBody: R, params?: HttpParams, apiUrl = this.apiUrl): Observable<T> {
    const fullUrl = this.getFullUrl(url, apiUrl);
    return this.proceedFullRequest<T>(this.httpClient.put<T>(fullUrl, requestBody, this.getOptions(params)), fullUrl);
  }
  patch<T, R>(url: string, requestBody: R, params?: HttpParams, apiUrl = this.apiUrl): Observable<T> {
    const fullUrl = this.getFullUrl(url, apiUrl);
    return this.proceedFullRequest<T>(this.httpClient.patch<T>(fullUrl, requestBody, this.getOptions(params)), fullUrl);
  }
  private proceedFullRequest<T>(request: Observable<T>, fullUrl?: string): Observable<T> {
    this.sharedService.setLoading(true);
    return request.pipe(
      map(res => {
        const response: Success<T> = new Success<T>(res, fullUrl);
        this.sharedService.setLoading(false);
        return res;
      }),
      catchError((err: any, caught: Observable<T>) => {
        this.layoutService.showError(err);
        this.sharedService.setLoading(false);
        return throwError(err);
      })
    );
  }

  buildQueryParams<TYPE>(object: TYPE): string {
    let url = '?';
    Object.keys(object).map((key: string, index: number) => {
      const value = object[key];
      if (index !== 0 && value) {
        url += '&';
      }
      if (value) {
        url += `${key}=${value}`;
      }
    });
    return url;
  }

  private getOptions(
    params: HttpParams
  ): {
    headers?:
      | HttpHeaders
      | {
          [header: string]: string | string[];
        };
    observe?: 'body';
    params?:
      | HttpParams
      | {
          [param: string]: string | string[];
        };
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
  } {
    // const token = this.accountQuery.getToken();
    return {
      headers: {
        // Authorization: ` Bearer ${token}`,
      },
      params,
    };
  }
}
