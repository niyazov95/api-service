import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {APIService} from './api.service';
import {CategoryModel} from '../modules/shared/models/category.model';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  selectedCategory = new Subject<CategoryModel[]>();
  selectedCategories: Subject<number[]> = new Subject<number[]>();
  constructor(private _api: APIService) {}
  handleCategorySelection(ids: number[], category: CategoryModel) {
    this.selectedCategories.next(CategoryModel.indicateIfExists(ids, category));
  }
  getParent() {
    return this._api.get('category/getParentCategories');
  }
  getChild(id) {
    return this._api.get(`category/getSubCategories/${id}`);
  }
  getInternalCategories(active: boolean = true) {
    return this._api.get(`internalCategory/GetCategories?active=${active}`);
  }
  getNewChild(id) {
    return this._api.get(`category/getNewSubCategories?categoryId=${id}`);
  }
  filterCategories(apiUrl: string, dto: {keyword: string; pageNumber: number; pageSize: number; active: boolean}) {
    const url = this._api.buildQueryParams<{keyword: string; pageNumber: number; pageSize: number}>(dto);
    return this._api.get(apiUrl + url);
  }
}
